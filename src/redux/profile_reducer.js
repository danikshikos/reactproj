const ADD_POST = "ADD-POST";
const UPDATE_NEW_POST_TEXT = "UPDATE-NEW-POST-TEXT";

let initialState = {
  //посты
  posts: [
    { id: 1, message: "Первый нах", klasscount: -25 },
    { id: 2, message: "Второй, жду третьего!", klasscount: 31 },
    { id: 3, message: "Третий, первый иди в попу", klasscount: 26 },
  ],
  //текст поста
  newPostText: "",
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    //добавление нового поста
    case ADD_POST:
      //объявление элемента поста и присвоение значений
      let newPost = {
        id: state.posts.length,
        message: state.newPostText,
        klasscount: 0,
      };
      //добавление поста в массив постов
      state.posts.push(newPost);
      //обнуление поля ввода текста поста
      state.newPostText = "";
      return state;
    //отслеживание изменения текста в текстарее при добалвении поста
    case UPDATE_NEW_POST_TEXT:
      //изменяет значение в хранилище
      state.newPostText = action.text;
      return state;
    default:
      return state;
  }
};

export const addPostActionCreator = () => ({
  type: ADD_POST,
});

export const updateNewPostTextActionCreator = (text) => ({
  type: UPDATE_NEW_POST_TEXT,
  text: text,
});

export default profileReducer;
