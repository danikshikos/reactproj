import dialogsReducer from "./dialogs_reducer";
import navbarReducer from "./navbar_reducer";
import profileReducer from "./profile_reducer";


let store = {
  //функция для присвоения ей функции обновления из индекса
  _subscriber() {
    console.log("без подпездчеков");
  },
  //хранилище данных
  _state: {
    
   
    //для навбара
    navbar: {
      //панель друзей
      friendsBar: [
        { id: 1, name: "Саня" },
        { id: 2, name: "Артем" },
        { id: 3, name: "Вика" },
      ],
    },
  },
  dispatcher(action) {
    this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
    this._state.profilesPage = profileReducer(this._state.profilesPage,action);
    this._state.navbar = navbarReducer(this._state.navbar,action);
    this._subscriber(this._state);
  },
  getState() {
    return this._state;
  },
  subscribe(observer) {
    this._subscriber = observer;
  },
};





export default store;
