const SEND_MESSAGE = "SEND-MESSAGE";
const UPDATE_NEW_MESSAGE_TEXT = "UPDATE-NEW-MESSAGE-TEXT";

let initialState = {
  //диалоги
  dialogs: [
    { id: 1, name: "Саня" },
    { id: 2, name: "Вика" },
    { id: 3, name: "Артём" },
    { id: 4, name: "Антон" },
    { id: 5, name: "Лёха" },
  ],
  //сообщения диалога
  messages: [
    { id: 1, message: "Хаха ебать" },
    { id: 2, message: "НУ ЧО НАРОД" },
    { id: 3, message: "ПОГНАЛИ НАХУЙ" },
    { id: 4, message: "ЕБАНЫЙ ВРОООТ" },
    { id: 5, message: "НЕ ЛЕЗЬ ДЕБИЛ" },
    { id: 6, message: "ОНО ТЕБЯ СОЖРЕТ" },
    { id: 7, message: "ААААААААААААА ПАЛКУ" },
  ],
  newMessageText: "",
};

const dialogsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEND_MESSAGE:
      let newMessage = {
        id: state.messages.length,
        message: state.newMessageText,
      };
      state.messages.push(newMessage);
      state.newMessageText = "";
      return state;
    case UPDATE_NEW_MESSAGE_TEXT:
      state.newMessageText = action.text;
      return state;
    default:
      return state;
  }
};

export const sendMessageActionCreator = () => ({
  type: SEND_MESSAGE,
});

export const updateNewMessageTextActionCreator = (text) => ({
  type: UPDATE_NEW_MESSAGE_TEXT,
  text: text,
});

export default dialogsReducer;
