import reportWebVitals from "./reportWebVitals";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import {Provider} from "react-redux";
import App from "./App";
import store from "./redux/redux_store";
import { BrowserRouter } from "react-router-dom";

//отрисовка страницы
let renderEntireTree = () => {
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
      <App/>
      </Provider>
    </React.StrictMode>,
    document.getElementById("root")
  );
};


//первоначальная отрисовка
renderEntireTree(store.getState());

//функция отрисовки в store = функция отрисовки здесь
store.subscribe(() => {
  let state = store.getState();
  renderEntireTree();
  }
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
