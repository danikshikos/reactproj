import React from "react";
import s from "./Header.module.css";

const Header = () => {
  return (
    <header className={s.header}>
      <img
        alt=""
        src="https://yandex.ru/images/_crpd/x5GI4y114/72f101bb/YODfddFMgQ9AYMN8aYJgkIG3g5MnzPM4XDnvDQyYP5hvubihkJ57Ta73EVi5o6oSEe8kVB8zsGPvWq87m_gjKiCV32Q0bcH6lPHGHPxHkyMiIXFQhHshP947S00pOE_sbg2c0gZPWsq_ULGLqrEtcNSLVEHaonZ0m10ZvHvZYG8DQIrgQtxDafOTJA1ZAqCwsIDyAYaqYA9cmdq_qYj_PbocbNCU64oIrBCCCJnQjQDGybbz67GUhQhPyw0ImHAM4jDYUSBMUYki4AYdq2JgUVBD8GHEmZGercv5jnlKHy7qLi3T1xspKo1QcGjLgX1itklUQ5izFkcrTSl-mVtzTTCxSuGSb0E5AsHFLNtTAOHRUxHCdRiRf2z4-b5qmjyvC4-f8SQrKzud81FbeMNOEURbVoAoY-Un-P_KfGt4It8RYYtBQCxwO4JTtQ6oAjHhc5Lj4dS7UN6ueghfSlqtfKjMPXFGqwkoD_CRagiQrRKE63TS61E0hBovG03ryhJ8o_L4sKA9AUjQc6f-2pKwIbIhQDO0OzI-PLjaTYlbjv-Lv89DV5k5O-4hsyoJoT2SNHiGQImi1zUoX7ttKJrg_AAzWUCjfvCqokH23unysPIg8xBx9ZmTvQ6Imm96m1zumB7u4yQ7qUiuolMaqWD8kQbLZIHIYLRUS38I_Xua4YzCgarwAH5DCfBhpay7QxGCcEPx4vX5ku_9OOlfm4jvjYo__KFk-sh63qIBGukQLjK3uvTw-2L0RGqNuz1riZFt8AK6wcBNIkiQYJfNSxIgkoLhstAl-TINHorKv5vrDQ27nh8DdJvbG-wQkjjJks4w9-p209oTlBTpnkjc6WmzPtKhqAAwPwPLMlLlPZlC4rCg8yFBBRmh_Ow66o67eB98yIyvoOU5utnMokKZyJA_kTVIxLOYscdlSF2qn-hJgk7hYtiz8vzB-YGB9q-LAuGSc6EwUHeooM_ce5peakpPc"
      />
    </header>
  );
};

export default Header;
