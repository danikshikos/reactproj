import React from "react";
import { NavLink } from "react-router-dom";
import StoreContext from "../../StoreContext";
import FriendsBar from "./FriendsBar/FriendsBar";
import s from "./Navbar.module.css";

const Navbar = (props) => {
  let state = props.state;

  let friendsbar = state.friendsBar.map((f) => (
    <FriendsBar id={f.id} name={f.name} />
  ));

  return (
    <nav className={s.navbar}>
      <div className={s.item}>
        <NavLink to="/profile" activeClassName={s.active}>
          Профиль
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink to="/dialogs" activeClassName={s.active}>
          Сообщения
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink to="/news" activeClassName={s.active}>
          Новости
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink to="/music" activeClassName={s.active}>
          Музыка
        </NavLink>
      </div>
      <div className={s.item}>
        <NavLink to="/settings" activeClassName={s.active}>
          Настройки
        </NavLink>
      </div>
      <div className={s.item}>Мои друзья</div>

      <div className={s.friendsbar}>{friendsbar}</div>
    </nav>
  );
};

export default Navbar;
