import React from "react";
import s from "./FriendsBar.module.css";

const FriendsBar = (props) => {
  return (
    <div className={s.friendBar} key={props.id}>
      <div className={s.friendBarImg}>
        <img
          src="https://pbs.twimg.com/profile_images/457079486417481728/_3dIT1vq.jpeg"
          alt=""
        ></img>
      </div>
      <div className={s.friendBarName}>{props.name}</div>
    </div>
  );
};

export default FriendsBar;
