import React from "react";
import s from "./ProfileInfo.module.css";

const ProfileInfo = () => {
  return (
    <div>
      <div>
        <img
          alt=""
          className={s.contentWall}
          src="https://photocentra.ru/images/main31/312829_main.jpg"
        ></img>
      </div>
      <div className={s.profDescr}>
        <img
          alt=""
          className={s.profileImg}
          src="https://avatars.mds.yandex.net/get-zen_doc/1362253/pub_5cf4d0bf6ba33d00afc8cc0b_5cf4d8eb9a90a100ae0543f2/scale_1200"
        />
        Обо мне
      </div>
    </div>
  );
};

export default ProfileInfo;
