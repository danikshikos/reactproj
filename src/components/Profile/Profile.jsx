import React from "react";
import Posts from "./Posts/Posts";
import PostsContainer from "./Posts/PostsContainer";
import s from "./Profile.module.css";
import ProfileInfo from "./ProfileInfo/ProfileInfo";

const Profile = (props) => {
  return (
    <div>
      <ProfileInfo />
      <PostsContainer />
    </div>
  );
};

export default Profile;
