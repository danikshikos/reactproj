import React from "react";
import s from "./Post.module.css";

const Post = (props) => {
  return (
    <div className={s.item} key={props.id}>
      <img
        src="https://ic.pics.livejournal.com/gamlet0/76715146/313308/313308_900.jpg"
        alt=""
      ></img>
      {props.message}
      <div>
        <span>Klassов: {props.klasscount} </span>
        <span>Ставлю klass </span>
      </div>
    </div>
  );
};

export default Post;
