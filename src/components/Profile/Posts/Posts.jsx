import React from "react";
import Post from "./Post/Post";
import s from "./Posts.module.css";

const Posts = (props) => {
  let state = props.state;
  //генерация объектов постов из массива в обратном порядке
  let postsItems = state.posts
    .reverse()
    .map((p) => (
      <Post id={p.id} message={p.message} klasscount={p.klasscount} />
    ));

  //ссылка на элемент
  let newPostElem = React.createRef();

  //вызов функции добавления
  let addPost = () => {
    props.addPost();
  };

  //вызов функции регистрации текста
  let changeNewPostText = () => {
    let text = newPostElem.current.value;
    props.changeNewPostText(text);
  };

  return (
    <div className={s.postBlock}>
      <h3>Посты</h3>
      <div>
        <div>
          <textarea
            ref={newPostElem}
            value={state.newPostText}
            onChange={changeNewPostText}
          ></textarea>
        </div>

        <div>
          <button onClick={addPost}>Опубликовать</button>
        </div>
      </div>
      <div className={s.posts}>{postsItems}</div>
    </div>
  );
};

export default Posts;
