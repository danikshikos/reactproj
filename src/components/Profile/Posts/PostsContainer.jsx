import { connect } from "react-redux";
import {
  addPostActionCreator,
  updateNewPostTextActionCreator,
} from "../../../redux/profile_reducer";
import Posts from "./Posts";

let mapStateToProps = (state) => {
  return {
    state: state.profilePage,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    addPost: () => {
      dispatch(addPostActionCreator());
    },
    //вызов функции регистрации текста
    changeNewPostText: (text) => {
      dispatch(updateNewPostTextActionCreator(text));
    },
  };
};

const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(Posts);

export default PostsContainer;
