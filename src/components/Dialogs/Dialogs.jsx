import React from "react";
import {
  sendMessageActionCreator,
  updateNewMessageTextActionCreator,
} from "../../redux/dialogs_reducer";
import Dialog from "./Dialog/Dialog";
import s from "./Dialogs.module.css";
import Message from "./Message/Message";

const Dialogs = (props) => {
  let state = props.state;

  //генерация объектов диалогов из массива
  let dialogsItems = state.dialogs.map((d) => (
    <Dialog id={d.id} name={d.name} />
  ));
  //генерация объектов сообщения из массива
  let messagesItems = state.messages.map((m) => (
    <Message id={m.id} message={m.message} />
  ));

  let sendMessage = () => {
    props.sendMessage();
  };

  let changeMessageText = (e) => {
    let messageText = e.target.value;
    props.changeMessageText(messageText);
  };

  return (
    <div className={s.dialogs}>
      <div className={s.dialogItems}>{dialogsItems}</div>

      <div className={s.messages}>
        {messagesItems}
        <div className={s.sendPanel}>
          <textarea
            onChange={changeMessageText}
            value={state.newMessageText}
            placeholder="Введите ваше сообщение..."
          ></textarea>
          <button onClick={sendMessage}>Отправить</button>
        </div>
      </div>
    </div>
  );
};

export default Dialogs;
