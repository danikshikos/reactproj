import React from "react";
import { NavLink } from "react-router-dom";
import s from "./Dialog.module.css";

const Dialog = (props) => {
  let path = "/dialogs/" + props.id;

  return (
    <div className={s.dialog}>
      <NavLink to={path}>
        <img
          src="https://s.starladder.com/uploads/user_logo/e/5/0/f/meta_tag_8ad84320724353fd3a704259ad5d08c8.png"
          alt=""
        ></img>
        {props.name}
      </NavLink>
    </div>
  );
};

export default Dialog;
